﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameInvaders
{
    class Shield : GameObject
    {
        private readonly int _index;

        public Shield(int index)
        {
            maxHp = 1;

            _index = index;
            texture = Global.content.Load<Texture2D>("spr_shield");
            Reset();
        }

        public override void Reset()
        {
            base.Reset();

            position.X = _index * 180 + 100;
            position.Y = Global.height - texture.Height - 100;
        }

        public override void Draw()
        {
            if (!isActive)
            {
                return;
            }

            base.Draw();

            Global.spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
