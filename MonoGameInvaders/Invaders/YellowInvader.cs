﻿namespace MonoGameInvaders.Invaders
{
    class YellowInvader : Invader
    {
        private int frameCount;

        public YellowInvader() : base("spr_yellow_invader")
        {
            
        }

        public override void Update()
        {
            base.Update();

            position.X += velocity.X;

            if (position.X > Global.width - texture.Width || position.X < 0)
            {
                position.X -= velocity.X;
                velocity.X = -velocity.X;
                position.Y += velocity.Y;
            }

            if (frameCount % 10 == 0)
            {
                if (frameCount % 20 == 0)
                {
                    position.Y -= 10;
                }
                else
                {
                    position.Y += 10;
                }
            }

            frameCount++;
        }
    }
}
