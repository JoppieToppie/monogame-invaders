﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameInvaders
{
    public class GameObject
    {
        public Vector2 position;
        public Vector2 velocity;
        public Texture2D texture;

        public bool isActive;

        public int maxHp = 2;
        public int currentHp;

        public GameObject()
        {
            Global.GameObjects.Add(this);
        }

        public virtual void Reset()
        {
            isActive = true;
            currentHp = maxHp;
        }

        public virtual void Update()
        {

        }

        public virtual void Draw()
        {

        }

        public virtual void TakeDamage(int amount)
        {
            currentHp -= amount;

            if (currentHp <= 0)
            {
                Die();
            }
        }

        public virtual void Die()
        {
            isActive = false;
        }

        public bool Overlaps(GameObject other)
        {
            float ownWidth = texture.Width,
                ownHeight = texture.Height,
                otherWidth = other.texture.Width,
                otherHeight = other.texture.Height,
                ownXPos = position.X,
                ownYPos = position.Y,
                otherXPos = other.position.X,
                otherYPos = other.position.Y;

            return !(ownXPos > otherXPos + otherWidth || ownXPos + ownWidth < otherXPos || ownYPos > otherYPos + otherHeight || ownYPos + ownHeight < otherYPos);
        }
    }
}
