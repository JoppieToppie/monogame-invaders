﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameInvaders
{
    class Boss : GameObject
    {
        public Boss()
        {
            maxHp = 2;

            texture = Global.content.Load<Texture2D>("spr_enemy_ship");
            Reset();
        }

        public override void Reset()
        {
            base.Reset();
            
            position.X = Global.Random(100, Global.width - 100);
            position.Y = 10.0f;

            velocity.X = 3.0f;
        }

        public override void Update()
        {
            if (!isActive)
            {
                return;
            }

            base.Update();

            position.X += velocity.X;

            if (position.X > Global.width - texture.Width || position.X < 0)
            {
                position.X -= velocity.X;
                velocity.X = -velocity.X;
            }
        }

        public override void Draw()
        {
            if (!isActive)
            {
                return;
            }

            base.Draw();

            Global.spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
