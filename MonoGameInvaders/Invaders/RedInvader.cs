﻿namespace MonoGameInvaders.Invaders
{
    class RedInvader : Invader
    {
        public RedInvader() : base("spr_red_invader")
        {
            
        }

        public override void Update()
        {
            base.Update();

            position.X += velocity.X;

            if (position.X > Global.width - texture.Width || position.X < 0)
            {
                position.X -= velocity.X;
                velocity.X = -velocity.X;
                position.Y += velocity.Y;
            }
        }
    }
}
