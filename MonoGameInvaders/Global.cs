﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGameInvaders
{
    static class Global
    {
        public static readonly List<GameObject> GameObjects = new List<GameObject>();

        public static ContentManager content;
        public static Rectangle screenRect;
        public static KeyboardState keys;
        public static int width, height;
        public static SpriteBatch spriteBatch;
        private static GraphicsDevice _graphicsDevice;

        public static GraphicsDevice GraphicsDevice
        {
            get => _graphicsDevice;

            set
            {
                _graphicsDevice = value;
                screenRect = new Rectangle(0, 0, _graphicsDevice.Viewport.Width, _graphicsDevice.Viewport.Height);
                width = screenRect.Width;
                height = screenRect.Height;
            }
        }

        private static readonly Random RGen = new Random();
        public static int Random(int lower, int upper)
        {
            return RGen.Next(lower, upper);
        }
    }
}
