﻿namespace MonoGameInvaders.Invaders
{
    class GreenInvader : Invader
    {
        public GreenInvader() : base("spr_green_invader")
        {
            
        }

        public override void Reset()
        {
            base.Reset();

            velocity.X = 1.5f;
            velocity.Y = 0.2f;
        }

        public override void Update()
        {
            base.Update();

            position += velocity;

            if (position.X > Global.width - texture.Width || position.X < 0)
            {
                position.X -= velocity.X;
                velocity.X = -velocity.X;
            }
        }
    }
}
