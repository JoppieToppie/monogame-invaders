﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameInvaders.Invaders;

namespace MonoGameInvaders
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MonoGameInvaders : Game
    {
        private const int InvaderAmount = 5;
        private const int ShieldAmount = 4;

        private SpriteBatch _spriteBatch;
        private Texture2D _background, _scanLines;

        public MonoGameInvaders()
        {
            var graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferHeight = 600,
                PreferredBackBufferWidth = 800
            };

            graphics.ApplyChanges();

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // Pass often referenced variables to Global
            Global.GraphicsDevice = GraphicsDevice;            
            Global.content = Content;

            // Create and Initialize game objects
            var player = new Player();
            new Bullet(player);
            new Boss();

            for (var i = 0; i < InvaderAmount; i++)
            {
                new YellowInvader();
                new BlueInvader();
                new GreenInvader();
                new RedInvader();
            }

            for (var i = 0; i < ShieldAmount; i++)
            {
                new Shield(i);
            }

            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            Global.spriteBatch = _spriteBatch;

            _background = Content.Load<Texture2D>("spr_background");
            _scanLines = Content.Load<Texture2D>("spr_scanLines");

            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            // Pass keyboard state to Global so we can use it everywhere
            Global.keys = Keyboard.GetState();

            if (Global.keys.IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            // Update the game objects
            Global.GameObjects.ForEach(gameObject => gameObject.Update());

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {            
            _spriteBatch.Begin();

            // Draw the background (and clear the screen)
            _spriteBatch.Draw(_background, Global.screenRect, Color.White);

            // Draw the game objects
            Global.GameObjects.ForEach(gameObject => gameObject.Draw());

            _spriteBatch.Draw(_scanLines, Global.screenRect, Color.White);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
