﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameInvaders.Invaders
{
    class Invader : GameObject
    {
        public Invader(string assetName)
        {
            maxHp = 1;
            texture = Global.content.Load<Texture2D>(assetName);

            Reset();
        }

        public override void Reset()
        {
            base.Reset();

            position.X = Global.Random(100, Global.width - 100);
            position.Y = Global.Random(50, Global.height - 300);

            velocity.X = 3.0f;
            velocity.Y = 10.0f;
        }

        public override void Update()
        {
            base.Update();

            // Invader to Shield collision
            foreach (var other in Global.GameObjects)
            {
                if (other.GetType() != typeof(Shield))
                {
                    continue;
                }

                if (other.isActive && Overlaps(other))
                {
                    Reset();
                    other.TakeDamage(1);
                }
            }
        }

        public override void Draw()
        {
            base.Draw();

            Global.spriteBatch.Draw(texture, position, Color.White);
        }

        public override void Die()
        {
            base.Die();

            Reset();
        }
    }
}
