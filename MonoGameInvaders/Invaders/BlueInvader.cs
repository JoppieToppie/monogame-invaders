﻿using System;

namespace MonoGameInvaders.Invaders
{
    class BlueInvader : Invader
    {
        private readonly int _moveOffset = Global.Random(0, 1000);
        private float time;

        public BlueInvader() : base("spr_blue_invader")
        {
            
        }

        public override void Update()
        {
            base.Update();

            position.X += (float)Math.Sin((time + _moveOffset) / 5) * 2.25f;
            position.Y += 0.2f;

            time += 0.1f;
        }
    }
}
