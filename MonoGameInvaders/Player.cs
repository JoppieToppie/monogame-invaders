﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGameInvaders
{
    class Player : GameObject
    {
        private const float MovementSpeed = 10.0f;
        public Player()
        {
            texture = Global.content.Load<Texture2D>("spr_ship");
            Reset();       
        }

        public override void Reset()
        {
            base.Reset();

            position.X = Global.width / 2; // horizontal center on screen
            position.Y = Global.height - texture.Height; // bottom of screen
        }

        public override void Update()
        {
            base.Update();

            // Assume player is not moving
            velocity.X = 0;

            // Alter velocity when keys are pressed
            if (Global.keys.IsKeyDown(Keys.Left))
            {
                velocity.X = -MovementSpeed;
            }

            if (Global.keys.IsKeyDown(Keys.Right))
            {
                velocity.X = MovementSpeed;
            }           

            position += velocity;

            // clamp the x-position to make sure it never goes out of screen bounds           
            position.X = MathHelper.Clamp(position.X, 0, Global.width - texture.Width);
        }

        public override void Draw()
        {
            base.Draw();

            Global.spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
