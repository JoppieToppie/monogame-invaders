﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGameInvaders
{
    class Bullet : GameObject
    {
        private readonly Player _player;
        public bool isFired;

        public Bullet(Player player)
        {
            _player = player;
            texture = Global.content.Load<Texture2D>("spr_bullet");

            Reset();
        }

        public override void Reset()
        {
            isFired = false;
            position.X = -1000;
            velocity.Y = 0;
        }

        public override void Update()
        {
            base.Update();

            if (Global.keys.IsKeyDown(Keys.Space))
            {
                Fire(_player.position);
            }

            if (isFired)
            {
                if (position.Y < 0)
                {
                    Reset();
                }
            }

            position.Y += velocity.Y;

            CheckCollision();
        }

        private void CheckCollision()
        {
            foreach (var other in Global.GameObjects)
            {
                if (other == this || other.GetType() == typeof(Player))
                {
                    continue;
                }

                if (other.isActive && Overlaps(other))
                {
                    Reset();
                    other.TakeDamage(1);
                }
            }
        }

        public override void Draw()
        {
            base.Draw();

            Global.spriteBatch.Draw(texture, position, Color.White);
        }

        public void Fire(Vector2 startPosition)
        {
            if (!isFired)
            {
                isFired = true;

                position.X = startPosition.X;
                position.Y = startPosition.Y;

                velocity.Y = -3.0f;
            }
        }
    }
}
